# uap

[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Documentation](https://docs.rs/uap/badge.svg)](https://docs.rs/uap)
[![Coverage](https://gitlab.com/jjwiseman/uap/badges/master/coverage.svg)]()

uap is a Rust crate that supports looking up aircraft registration and type
information from the FAA database and or Mictronics database.