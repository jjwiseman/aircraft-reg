//! Loads a Mictronics database.

use serde::Deserialize;
use std::{collections::HashMap, fs::File, io::BufReader, path::Path};

use crate::{Database, Error, Reg, Source};

// {
//     "004001": {
//         "d": "Ilyushin IL-76 T",
//         "f": "00",
//         "r": "Z-WTV",
//         "t": "IL76"
//     },
//     "004002": {
//         "d": "Boeing 737 2N0",
//         "f": "00",
//         "r": "Z-WPA",
//         "t": "B732"
//     }
// }

#[derive(Deserialize)]
struct Aircraft {
    d: String,
    r: String,
}

/// The Mictronics aircraft database.
pub struct Mictronics {
    mode_s_index: HashMap<String, Aircraft>,
}

impl Mictronics {
    /// Load a database from a directory containing Mictronics JSON files.
    pub fn from_dir(dir: &Path) -> Result<Self, Error> {
        let mut aircrafts_path = dir.to_path_buf();
        aircrafts_path.push("aircrafts.json");

        let file = File::open(aircrafts_path.clone()).map_err(|e| {
            Error::DatabaseLoadError(format!(
                "Error reading Mictronics JSON file {}: {}",
                aircrafts_path.display(),
                e
            ))
        })?;
        let reader = BufReader::new(file);
        let mode_s_index = serde_json::from_reader(reader).map_err(|e| {
            Error::DatabaseLoadError(format!(
                "Error parsing Mictronics JSON file {}: {}",
                aircrafts_path.display(),
                e
            ))
        })?;
        Ok(Self { mode_s_index })
    }
}

impl Database for Mictronics {
    fn lookup_mode_s(&self, hex: &str) -> Result<Option<Reg>, Error> {
        let hex = hex.to_uppercase();
        match self.mode_s_index.get(&hex) {
            Some(a) => Ok(Some(Reg {
                hex_mode_s: Some(hex),
                registration: Some(a.r.clone()),
                kind: Some(a.d.clone()),
                sources: vec![Source::Mictronics],
                registrant: None,
            })),
            None => Ok(None),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::*;

    macro_rules! assert_err {
        ($expression:expr, $($pattern:tt)+) => {
            match $expression {
                $($pattern)+ => (),
                ref e => panic!("expected `{}` but got `{:?}`", stringify!($($pattern)+), e),
            }
        }
    }

    #[test]
    fn test_lookup() {
        let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        d.push("tests/data/mictronics");
        eprintln!("{}", d.display());
        let db = Mictronics::from_dir(&d).unwrap();
        let r = db.lookup_mode_s("a19fa9").unwrap().unwrap();
        assert_eq!(
            r,
            Reg {
                hex_mode_s: Some("A19FA9".to_string()),
                registration: Some("N2031F".to_string()),
                registrant: None,
                kind: Some("1975 BALLOON WORKS FIREFLY 7".to_string()),
                sources: vec![Source::Mictronics]
            }
        );
    }

    #[test]
    fn test_lookup_err() {
        let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        d.push("tests/data/does-not-exist");
        let db = Mictronics::from_dir(&d);
        assert_err!(db.err(), Some(Error::DatabaseLoadError(_)));
    }
}
