use std::path::PathBuf;
use structopt::StructOpt;
use uap::{faa::Faa, mictronics::Mictronics, Database};

#[derive(StructOpt, Debug)]
struct Args {
    #[structopt(long, value_name = "DIR")]
    faa_dir: Option<PathBuf>,
    #[structopt(long, value_name = "PATH")]
    mictronics_file: Option<PathBuf>,
    hex: String,
}

fn main() {
    let args = Args::from_args();
    let faa = args.faa_dir.map(|dir| Faa::from_dir(&dir).unwrap());
    let mictronics = args
        .mictronics_file
        .map(|path| Mictronics::from_dir(&path).unwrap());
    let hex = args.hex;
    if let Some(faa) = faa {
        println!("FAA:\n{:#?}\n", faa.lookup_mode_s(&hex));
    }
    if let Some(mictronics) = mictronics {
        println!("Mictronics:\n{:#?}\n", mictronics.lookup_mode_s(&hex));
    }
}
