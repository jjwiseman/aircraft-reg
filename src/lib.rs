//! This crate provides a unified interface to several sources of information on
//! aircraft worldwide, including registration and type information.
//!
//! The main use case is finding an aircraft's registration ("tail number") if
//! you have its ICAO code (AKA Mode S code). ICAO codes can be broadcast by
//! aircraft transponders, so if you're looking at a transponder data packet and
//! want to find out more about the aircraft that sent it, the first step is
//! often to find its registration.
//!
//! This crate currently supports two source of information, both of which can
//! be downloaded by the public:
//!
//! * The [FAA registration
//!   database](https://www.faa.gov/licenses_certificates/aircraft_certification/aircraft_registry/releasable_aircraft_download/).
//!   The FAA database has information on all aircraft in the United States with
//!   civilian registrations.
//! * The [Mictronics
//!   database](https://www.mictronics.de/aircraft-database/export.php);
//!   Specifically, the "Complete database for old readsb (non protocol buffer
//!   version)" zip file that's contained at [this
//!   link](https://www.mictronics.de/aircraft-database/indexedDB_old.php). The
//!   Microntronics database tries to collect information on all aircraft in the
//!   world, including military aircraft.
//!
//! To use this crate, you need to
//! 1. Download the FAA database zip, or the Mictronics database, or both.
//! 2. Unzip the database(s) into a directory.
//! 3. Call [`faa::Faa::from_dir()`] or [`mictronics::Mictronics::from_dir()`]
//!    with the directory containing the database files.
//! 4. Call the resulting struct's `lookup_mode_s` function with the Mode S code
//!    of the aircraft you want information on.
//!
//! ```
//! use uap::Database;
//! use uap::faa::Faa;
//! use uap::mictronics::Mictronics;
//! use std::str::FromStr;
//!
//! // ./tests/data/faa is a directory containing the FAA CSV files.
//!
//! let faa = Faa::from_dir(&std::path::PathBuf::from_str("./tests/data/faa").unwrap()).unwrap();
//! let aircraft = faa.lookup_mode_s("A44360").unwrap().unwrap();
//! assert_eq!(aircraft.registration, Some("N374HK".to_string()));
//! assert_eq!(aircraft.registrant.unwrap().name, "GENERAL ATOMICS AERONAUTICAL SYSTEMS INC".to_string());
//! assert_eq!(aircraft.kind, None);
//!
//! // ./tests/data/mictronics is a directory containing the FAA CSV files.
//!
//! let mictronics = Mictronics::from_dir(
//!   &std::path::PathBuf::from_str("./tests/data/mictronics").unwrap(),
//! )
//! .unwrap();
//! let aircraft = mictronics.lookup_mode_s("A44360").unwrap().unwrap();
//! assert_eq!(aircraft.registration, Some("N374HK".to_string()));
//! assert_eq!(aircraft.kind, Some("UHK97000-12".to_string()));
//! ```
use merge::Merge;
pub mod errors;
pub mod faa;
pub mod mictronics;

use crate::errors::Error;

/// Possible sources of information.
#[non_exhaustive]
#[derive(Debug, PartialOrd, PartialEq, Ord, Eq)]
pub enum Source {
    /// The FAA registration database.
    Faa,
    /// The Mictronics database.
    Mictronics,
}

/// Registration information.
#[derive(Debug, PartialOrd, PartialEq, Eq, Merge)]
pub struct Reg {
    /// The "tail number".
    pub registration: Option<String>,
    /// The ICAO/Mode S code.
    pub hex_mode_s: Option<String>,
    /// The entity the aircraft is registered to.
    pub registrant: Option<Registrant>,
    /// The type of aircraft.
    pub kind: Option<String>,
    /// The sources of the information in this struct.
    #[merge(strategy = merge::vec::append)]
    pub sources: Vec<Source>,
}

/// Entity an aircraft is registered to.
#[derive(Debug, PartialOrd, PartialEq, Ord, Eq)]
pub struct Registrant {
    /// The entity's name.
    pub name: String,
}

/// Trait for looking up aircraft information.
pub trait Database {
    /// Looks up an aircraft by its ICAO/Mode S code.
    fn lookup_mode_s(&self, hex: &str) -> Result<Option<Reg>, Error>;
}

pub struct MergeDatabase<'a> {
    databases: Vec<&'a dyn Database>,
}

impl<'a> MergeDatabase<'a> {
    pub fn new() -> Self {
        MergeDatabase {
            databases: Vec::new(),
        }
    }

    pub fn add_database(&mut self, db: &'a dyn Database) {
        self.databases.push(db);
    }
}

impl<'a> Database for MergeDatabase<'a> {
    fn lookup_mode_s(&self, hex: &str) -> Result<Option<Reg>, Error> {
        let mut r: Option<Reg> = None;
        for d in self.databases.iter() {
            match r {
                Some(_) => r.merge(d.lookup_mode_s(hex)?),
                None => r = d.lookup_mode_s(hex)?,
            };
        }
        Ok(r)
    }
}

impl Default for MergeDatabase<'_> {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_merge() {
        let mut a = Reg {
            hex_mode_s: Some("FEED00".to_string()),
            registration: Some("N1234".to_string()),
            registrant: Some(Registrant {
                name: "JJW".to_string(),
            }),
            kind: None,
            sources: vec![Source::Faa],
        };
        let b = Reg {
            hex_mode_s: Some("FEED00".to_string()),
            registration: Some("K-PAB".to_string()),
            registrant: None,
            kind: Some("AS.350".to_string()),
            sources: vec![Source::Mictronics],
        };
        a.merge(b);
        assert_eq!(
            a,
            Reg {
                hex_mode_s: Some("FEED00".to_string()),
                registration: Some("N1234".to_string()),
                registrant: Some(Registrant {
                    name: "JJW".to_string(),
                }),
                kind: Some("AS.350".to_string()),
                sources: vec![Source::Faa, Source::Mictronics],
            }
        );
    }
}
