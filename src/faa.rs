//! Loads an FAA registration database.

use std::{collections::HashMap, path::Path};

use csv::StringRecord;

use crate::{Database, Error, Reg, Registrant, Source};

/// The FAA registration database.
#[derive(Debug, PartialEq, Eq)]
pub struct Faa {
    mode_s_map: HashMap<String, StringRecord>,
}

impl Faa {
    /// Load a database from a directory containing FAA CSV files.
    pub fn from_dir(dir: &Path) -> Result<Self, Error> {
        let mut mode_s_map = HashMap::new();
        let mut master_path = dir.to_path_buf();
        master_path.push("MASTER.txt");
        let mut rdr = csv::Reader::from_path(master_path.clone()).map_err(|e| {
            Error::DatabaseLoadError(format!(
                "Error reading FAA CSV file {}: {}",
                master_path.display(),
                e
            ))
        })?;
        for result in rdr.records() {
            let r = result.map_err(|e| {
                Error::DatabaseLoadError(format!(
                    "Error parsing FAA CSV file {}: {}",
                    master_path.display(),
                    e
                ))
            })?;
            if let Some(mode_s) = r.get(33) {
                mode_s_map.insert(mode_s.trim().to_uppercase(), r);
            }
        }
        Ok(Faa { mode_s_map })
    }
}

impl Database for Faa {
    fn lookup_mode_s(&self, hex: &str) -> Result<Option<Reg>, Error> {
        match self.mode_s_map.get(&hex.to_uppercase()) {
            Some(rec) => {
                let registration = rec.get(0).map(|s| format!("N{}", s));
                let registrant_name = rec.get(6).map(|s| s.trim());
                Ok(Some(Reg {
                    registration,
                    hex_mode_s: Some(hex.to_uppercase()),
                    registrant: registrant_name.map(|name| Registrant {
                        name: name.to_string(),
                    }),
                    kind: None,
                    sources: vec![Source::Faa],
                }))
            }
            None => Ok(None),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::PathBuf;

    macro_rules! assert_err {
        ($expression:expr, $($pattern:tt)+) => {
            match $expression {
                $($pattern)+ => (),
                ref e => panic!("expected `{}` but got `{:?}`", stringify!($($pattern)+), e),
            }
        }
    }

    #[test]
    fn test_lookup() {
        let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        d.push("tests/data/faa");
        let db = Faa::from_dir(&d).unwrap();
        let r = db.lookup_mode_s("a36622").unwrap().unwrap();
        assert_eq!(
            r,
            Reg {
                hex_mode_s: Some("A36622".to_string()),
                registration: Some("N318SJ".to_string()),
                registrant: Some(Registrant {
                    name: "OBR LEASING".to_string()
                }),
                kind: None,
                sources: vec![Source::Faa]
            }
        );
    }

    #[test]
    fn test_lookup_err() {
        let mut d = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        d.push("tests/data/does-not-exist");
        let db = Faa::from_dir(&d);
        assert_err!(db.err(), Some(Error::DatabaseLoadError(_)));
    }
}
